// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// Екранування дозволяє запобігти велікій кількості перевірок 
// на несанкціоноване введення користувачем якихось символів або на розпізнавання символів, наприклад ''.


// 2. Які засоби оголошення функцій ви знаєте?
// Function declaration, Function expression, Arrow function

// 3. Що таке hoisting, як він працює для змінних та функцій?
// Огошення функцій і змінних попадають в пам'ять в процессі фази компіляції, але залишаються 
// в коді на тому місті де були оголошені. Тобто піднімаються вгору по своїй області видимості перед виконанням коду.
// Але це може призвести до непорозумінь, бо у JS навпаки йшли до чіткого розмежування локальної 
//  і глобальної області видимості.




// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. 
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
// При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
// Створити метод getAge() який повертатиме скільки користувачеві років.
// Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем 
// (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.


const createNewUser = () => {
    let firstName = prompt("Enter your first name");
    let lastName = prompt("Enter your last name");
    let birthday = prompt("Enter you birthday", `dd.mm.yyyy`);
    let today = new Date();

    let newUser = {
        firstName,
        lastName,
        birthday,
        }; 
        newUser.getLogin = () => {
            return `${(newUser.firstName.slice(0, 1) + newUser.lastName).toLowerCase()}`;
        };
        newUser.getAge = () => {
            const birthdate = new Date(birthday.split('.').reverse().join('-'));
            let age = today.getFullYear() - birthdate.getFullYear();
            let month = today.getMonth() - birthdate.getMonth();
            if (month < 0 || (month === 0 && today.getDate() < birthdate.getDate()))
                age--

            return age;
        };
        newUser.getPassword = () => {
            return `${newUser.firstName.slice(0, 1).toUpperCase() + newUser.lastName.toLowerCase() + newUser.birthday.slice(6)}`
        };
    return newUser
}

let finalUser = createNewUser();
console.log(finalUser.getLogin());
console.log(finalUser.getAge());
console.log(finalUser.getPassword());


